
let todoUsers = [];
let todoTasks = [];

function addUser(...args) {
    let [firstName, LastName] = args;
    let users = {};
    let userName = `${firstName} ${LastName}`;

    // Return count of milliseconds from current date
    let createdAt = Date.now();
    let tasks = [];

    // If we add first user
    if (!todoUsers.length) {
        users = {firstName, LastName, createdAt, tasks};
        todoUsers.push(users);

        (() => console.log(`Юзер: ${userName} добавлен!`))();
    } else {
        // Get obj of users
        let usersObj = getUsersAsObj(todoUsers);

        if (usersObj[userName]) {
            console.log(`Юзер: ${userName} уже существует!`);
        } else {
            users = {firstName, LastName, createdAt, tasks};
            todoUsers.push(users);
            console.log(`Юзер: ${userName} добавлен!`);
        }
        /*
        for (let i = 0; i < todoUsers.length; i++) {
            if (todoUsers[i]['firstName'] == firstName && todoUsers[i]['LastName'] == LastName) {
                //console.log(`Юзер: ${firstName} ${LastName} уже существует!`);
                break;
            } else {
                //users = {firstName, LastName, createdAt};
                //todoUsers.push(users);
                //console.log(`Юзер: ${firstName} ${LastName} добавлен!`);
            }
        }
        */
    }

    //console.log(firstName, LastName, createdAt);
}

/* Function return obj of users
 * from array as key: "firstName LastName"
 */
function getUsersAsObj(userArray) {
    let usersObj = {};

    // forearch all user obj in array
    for (let user of userArray) {
        // Create new obj of uniq users
        usersObj[user['firstName'] + ' ' + user['LastName']] = true;
    }
    return usersObj;
}

/* Function return obj of tasks
 * from array as key: "title"
 */
function getTasksAsObj(tasksArray) {
    let tasksObj = {};

    // forearch all task obj in array
    for (let task of tasksArray) {
        // Create new obj of tasks
        tasksObj[task['title']] = true;
    }
    return tasksObj;
}

function addTask(...args) {
    let [author, title, description, deadline, responsible, status] = args;
    let tasks = {};

    // Return count of milliseconds from current date
    let createdAt = Date.now();

    /* Return TimeStamp in milliseconds
     * NOTE: bad code, use: getDeadlineInTimestamp() instead
     */
    deadline = (() => {
      let [date, month, year] = deadline.split('-');
      return new Date(year, month, date, 0, 0, 0, 0).getTime();
    })();

    // Check to user exist
    author = (() => {
        if (!getUsersAsObj(todoUsers)[author]) {
            return;
        }
        return author;
    })();

    if (!author) {
        console.log(`Пользователя не существует!`);
        return;
    }
    // if argument responsible not defined: responsible = author
    //responsible = (!responsible) ? from : responsible;
    responsible = (() => (!responsible) ? author : responsible)();
    status = status || 'Wait';

    // If we add first tasks
    if (!todoTasks.length) {
        tasks = {author, responsible, title, description, createdAt, deadline, status};
        todoTasks.push(tasks);

        // NOTE: write function to do this shit!!!
        for (let u of todoUsers) {
          if (u['firstName'] == author.split(' ')[0] && u['LastName'] == author.split(' ')[1]) {
            u['tasks'].push(title);
          }
        }

        (() => console.log(`Задача: ${description} добавлена!`))();
    } else {
        // Get obj of users
        let tasksObj = getTasksAsObj(todoTasks);

        if (tasksObj[title]) {
            console.log(`Задача: ${title} уже существует!`);
        } else {
            tasks = {author, responsible, title, description, createdAt, deadline, status};
            todoTasks.push(tasks);

            // NOTE: write function to do this shit!!!
            for (let u of todoUsers) {
              if (u['firstName'] == author.split(' ')[0] && u['LastName'] == author.split(' ')[1]) {
                u['tasks'].push(title);
              }
            }

            console.log(`Задача: ${description} добавлена!`);
        }
    }
}


function setResponsible(...args) {
    let [title, responsible] = args;

    // Check to user exist
    responsible = (() => {
        if (!getUsersAsObj(todoUsers)[responsible]) {
            return;
        }
        return responsible;
    })();

    if (!responsible) {
        console.log(`Пользователя не существует!`);
    } else {
        for (let task of todoTasks) {
            if ([task['title']] == title) {
                task['responsible'] = responsible;
                console.log(`Ответственный за задачу: ${title} изменен на: ${responsible}`);
                break;
            }
        }
    }
}

function getDeadlineInTimestamp(deadline) {
  let [date, month, year] = deadline.split('-');
  return new Date(year, month, date, 0, 0, 0, 0).getTime();
}

function setDeadline(...args) {
    let [title, deadline] = args;
    deadline = getDeadlineInTimestamp(deadline);

    for (let task of todoTasks) {
        if ([task['title']] == title) {
            task['deadline'] = deadline;
            deadline = formatDate(deadline);

            console.log(`'Дедлайн задачи: ${title} изменен на ${deadline}'`);
            break;
        }
    }
}

function showTasks(status) {
  /* ['title:	homework;	description:	do	math;	status:	Wait;
   * deadline:	23-06-2018;	responsible:	Petya	Petrov']
   */
  let resTasks = [];
  let resAllTasks= [];

  // View only setting status
  for (let task of todoTasks) {
    let deadline = formatDate(task['deadline']);
    // View only setting status
    if (status) {
      if (task['status'] == status) {
        resTasks.push(`title: ${task['title']}; description: ${task['description']}; status: ${task['status']}; deadline: ${deadline}; responsible: ${task['responsible']}`);
        break;
      }
    } else {
        resAllTasks.push(`title: ${task['title']}; description: ${task['description']}; status: ${task['status']}; deadline: ${deadline}; responsible: ${task['responsible']}`);
    }
  }


  // View only setting status
  if (resTasks.length) {
    console.log(resTasks);
  // Show all tasks without parameters
  } else {
    if (resAllTasks.length) {
      console.log(resAllTasks);
    }
  }
}

function showUsers() {
  /* ['Petya	Petrov;	registration:	20-01-2018;	tasks:	3',	'Alex
	Petrov;	registration:	20-03-2018;	tasks:	10']
  */
  let res = [];

  for (let user of todoUsers) {
    let regDate = formatDate(user['createdAt']);
    res.push(`${user['firstName']} ${user['LastName']}; registration: ${regDate}; tasks: ${user['tasks'].length}`);
  }
  console.log(res);
}

/* Function return formatted (date-month-year)
 * date string from TimeStamp
 */
function formatDate(time) {
  time = new Date(time);
  let [date, month, year] = [time.getDate(), time.getMonth(), time.getFullYear()];

  date = (date < 10) ? '0' + date : date;
  month = (month < 10) ? '0' + month : month;

  return `${date}-${month}-${year}`;
}

function setStatus(title,	status) {
  let stat = {
    'Wait': true,
    'In	Progress': true,
    'Done': true
  };

  if (status in stat) {
    for (let task of todoTasks) {
      if (task['title'] == title) {
        task['status'] = status;
        console.log(`Статус задачи: ${title} изменен на: ${status}`);
        break;
      }
    }
  }
}

addUser('a', 'aa');
addUser('a', 'aa');
addUser('b', 'bb');
addUser('b', 'bb');
addUser('c', 'cc');
addTask('a aa','t1', 'desc1', '02-04-2018','c', 'Done');
addTask('a aa','t2', 'desc2', '02-04-2018');
addTask('a aa','t2', 'desc2', '02-04-2018');
addTask('b bb','t3', 'desc3', '02-04-2018');
addTask('d dd','t4', 'desc3', '02-04-2018');
setResponsible('t1','a aa');
setResponsible('t1','d dd');
setDeadline('t1','21-04-2018');
setDeadline('t123','21-04-2018');
setStatus('t3','Done');
showTasks('Done');
//showTasks();
showUsers();

//console.log(todoUsers);
//console.log(todoTasks);
